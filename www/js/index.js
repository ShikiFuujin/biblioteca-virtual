var app = angular.module("appModule", []);
app.controller(
    "appController"
    , ($scope, $timeout, $http, $sce) => {
        $scope.formData = {};
        $scope.tesisList = [];
        $scope.tesisObj = {};
        $scope.coverOpen = false;
        $scope.buscar = function() {

            var identificador   = $scope.formData.identificador || ""
                , autor         = $scope.formData.autor || ""
                , descripcion   = $scope.formData.descripcion || ""
            ;

            var queryWhere = [];

            if(identificador) { queryWhere.push("identificador=" + identificador); }
            if(autor) { queryWhere.push("autor=" + autor); }
            if(descripcion) { queryWhere.push("descripcion=" + descripcion); }

            var url = "/info" + (queryWhere ? ("?" + queryWhere.join("&")) : "");

            $http.get(url)
                .then(
                    function(_result) {
                        $scope.tesisList = (_result.data || {}).data || [];
                    }
                    , function(_err) {
                        console.log(_err);
                    }
                );
        }

        $scope.abrirTesis = function(_tesis) {
            ($scope.tesisList || [])
                .forEach((_item) => {
                    _item.open = false;
                });

            _tesis.open = true;
            _tesis.documento = $sce.trustAsResourceUrl("/documento/" + _tesis.tesis_identificador.replace(/\s+\/\s+/g, " - ") + ".pdf");
            $scope.tesisObj = _tesis;
        }

        $scope.openCover = function() {
            $scope.coverOpen = !$scope.coverOpen;
        }

        $scope.buscar();

        $timeout(() => {
            $scope.coverOpen = true;
            $timeout(() => {
                $scope.coverOpen = false;
            }, 10000);
        }, 3000);
    });
