-- CREATE USER 'biblioteca'@'localhost' IDENTIFIED BY 'A9X5e2H4Nd88xak72FP*';

-- GRANT ALL PRIVILEGES ON * . * TO 'biblioteca'@'localhost';

-- FLUSH PRIVILEGES;

-- En caso de que hay un error de conexion por el tipo de autenticacion , toca editar el usuario para que lo permita por medio de mysql_native_password
-- ALTER USER 'biblioteca'@'localhost' IDENTIFIED WITH mysql_native_password BY 'A9X5e2H4Nd88xak72FP*';


CREATE DATABASE IF NOT EXISTS bd_biblioteca;

USE bd_biblioteca;



CREATE TABLE IF NOT EXISTS tb_tesis (
    tesis_codigo INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY
    , tesis_identificador VARCHAR(30) NOT NULL
    , tesis_autor TEXT NOT NULL
    , tesis_descripcion TEXT NOT NULL
    , tesis_fechacreacion DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
    , tesis_fechamodificacion DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);
