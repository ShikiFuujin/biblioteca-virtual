var db = require("../models/db.js");

const getInfo = (req, res) => {
    var queryUrl        = req.query || {}
        , identificador = queryUrl.identificador || ""
        , autor         = queryUrl.autor || ""
        , descripcion   = queryUrl.descripcion || ""
        ;

    var queryWhere = "";

    queryWhere += identificador ? (" AND LOWER(t.tesis_identificador) LIKE LOWER('%" + identificador + "%')") : "";
    queryWhere += autor ? (" AND LOWER(t.tesis_autor) LIKE LOWER('%" + autor + "%')") : "";
    queryWhere += descripcion ? (" AND LOWER(t.tesis_descripcion) LIKE LOWER('%" + descripcion + "%')") : "";
    db.query(
        "SELECT * FROM tb_tesis t WHERE TRUE " + queryWhere + " ORDER BY t.tesis_identificador ASC LIMIT 20;"
        , function (err, result) {
            if(err) {
                res.json({
                    success: false
                    , data: []
                    , msg: "Error ejecutando sentencia SQL"
                });
            } else {
                res.json({
                    success: true
                    , data: result
                    , msg: "OK"
                });
            }
        });

};

module.exports = {
    getInfo
};
