const { Router } = require("express")
    , router = Router()
    , { getInfo } = require("../controllers/controller.js")
;

router.get( "/info" , getInfo );

module.exports = router;
