var express = require('express')
    , app = express()
    , port = process.env.PORT || 3000;

// middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


//routes
app.use(express.static(__dirname + '/www'));
app.use(require("./api/routes/route.js"));

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
