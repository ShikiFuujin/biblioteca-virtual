# BIBLIOTECA VIRTUAL

Tecnologias usadas

Angularjs v 1.7.9
SASS 1.26.5
NodeJS 10.19.0
MySQL 8.0.20


Preparación

El proyecto se puede descargar en https://gitlab.com/ShikiFuujin/biblioteca-virtual

1. Crear la bd en el servidor de MySQL ( https://dev.mysql.com/downloads/windows/installer/5.7.html ) con los datos correspondientes al archivo " sql/estructura.sql "
1.1. Las credenciales de acceso y el nombre de la bd pueden cambiar sin embargo, se deben actualizar en el archivo " api/models/db.js "
1.2. Se insertan los datos del archivo " sql/insert.sql "
2. Se debe tener el servidor de Nodejs ( https://nodejs.org/es/download/ )
3. Para tener el demonio corriendo se debe instalar forever ( https://www.npmjs.com/package/forever ) ( en consola con npm install forever -g )
4. Ejecutar con forever el servidor para levantarlo ejemplo: forever start server.js
4.1. En el archivo " server.js " se encuentra parametrizado el puerto, este se puede modificar
5. El proyecto hace referencia a los pdfs solicitando la URL ( localhost:3000/documento/xxxxx.pdf ); por lo tanto, la carpeta documento dentro del servidor puede ser fisico con los pdfs ó, puede ser un enlace simbolico ( Acceso directo en windows ) el cual contiene los pdfs



PAra el auto run on start up de windows se debe ejecutar "shell:startup", y crear un acceso directo al archivo auto_start.vbs desde ahí. Nota: Acomodar la ruta del script del arhcivo 
